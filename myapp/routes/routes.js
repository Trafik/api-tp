const express = require ('express');

const _ = require('lodash');

const router = express.Router();

//CREATE

const films = [{
    name : "Star Wars",
    id : 0,
},
    {
        name : "Le seigneur des anneaux",
        id : 1,
    }
];

//GET ALL

router.get("films", res,res =>
{
    res.send(films)
});

//GET BY ID

router.get('/:id', (req, res) => {
    const {id} = req.params;

    const name = _.find(films, ["id", id]);

    res.status(200).json({
        message : 'Film found!',
        name
    });
});

//ADD

router.put('/', (req,res) => {
    const { movie } = req.body;
    const id = _.newID();
    films.push({film,id});
    res.json({message: '${id} added', titre: {movie,id}});

});

//UPDATE

router.post('/id',(req,res)=> {

    const { id } = req.params;
    const { movie } = req.body;
    const movieChange = _.find(films,["id",id]);
    movieChange.movie = movie;
    res.json({message: 'Update done'});
});

//DELETE

router.delete('/:id',(req,res) => {

    const { id } = req.params;
    _.remove(films,["id",id]);
    res.json({message: "${id} deleted"});
});


module.exports = router;